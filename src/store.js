import Vue from "vue";
import Vuex from "vuex";
import router from "./router";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userInfo: null,
    allUsers: [
      { id: 1, name: "myborn", email: "myborn@email.com", password: "123456" }
    ],
    isLogin: false,
    isLoginError: false
  },
  mutations: {
    loginSuccess(state, seletedUser) {
      state.isLogin = true;
      state.isLoginError = false;
      state.userInfo = seletedUser;
    },
    loginError(state) {
      state.isLogin = false;
      state.isLoginError = true;
    },
    logout(state) {
      state.isLogin = false;
      state.isLoginError = false;
      state.userInfo = null;
    }
  },
  actions: {
    // https://reqres.in/ fake rest API 를 통해 테스트
    login({ commit, dispatch }, loginObj) {
      axios
        .post("https://reqres.in/api/login", loginObj)
        .then(res => {
          let token = res.data.token;
          localStorage.setItem("access_token", token);
          dispatch("getMemberInfo");
        })
        .catch(() => {
          commit("loginError");
        });
    },
    logout({ commit }) {
      commit("logout");
      router.push({ name: "login" });
    },
    getMemberInfo({ commit }) {
      let config = {
        headers: {
          "access-token": localStorage.getItem("access_token")
        }
      };
      axios
        .get("https://reqres.in/api/users/2", config)
        .then(response => {
          let userInfo = {
            id: response.data.data.id,
            first_name: response.data.data.first_name,
            last_name: response.data.data.last_name,
            avatar: response.data.data.avatar
          };
          commit("loginSuccess", userInfo);
          router.push({ name: "mypage" });
        })
        .catch(() => {
          commit("loginError");
        });
    }
  }
});
