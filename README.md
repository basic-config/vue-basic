# vue-basic

## vue 설치 및 update
~~~
npm install -g @vue/cli
~~~

## 프로젝트 만들기
~~~
vue create vue-basic
~~~

## 프로젝트에서 외부라이브러리 추가
~~~
npm install [package] --save
npm install vuex --save
~~~
--save 를 통해 package dependencies에 명시한다
--save-dev package devDependencies에 명시한다
https://www.npmjs.com/ 에서 다양한 패키지를 검색하여 사용할수 있다.


## vuetify 추가
~~~
vue add vuetify
~~~

## axios install
~~~
npm install axios --save
~~~

---
---


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
